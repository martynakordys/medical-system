﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/userrole")]
    public class UserRoleController: ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IUserRoleService userRoleService;

        public UserRoleController(IUserProviderService userProviderService, 
            IUserService userService, 
            IUserRoleService userRoleService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.userRoleService = userRoleService;
        }

        [Authorize]
        [HttpGet("admin/getroles")]
        public IActionResult GetUserRoles()
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsAdmin(userId))
            {
                RoleListDTO roleListDTO = userRoleService.GetRoles();
                return Ok(roleListDTO);
            }
            else
            {
                return BadRequest("Brak uprawnien administratora");
            }

        }
    }
}
