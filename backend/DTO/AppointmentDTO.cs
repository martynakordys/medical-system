﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.DTO
{
    public class AppointmentDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Diagnose { get; set; }
        public string Status { get; set; }
        public string FinishedCancelledDate { get; set; }
        public string DoctorName { get; set; }
        public string PatientName { get; set; }
        public string RegistrantName { get; set; }
    }
}
