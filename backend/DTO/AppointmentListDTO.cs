﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.DTO
{
    public class AppointmentListDTO
    {
        public List<AppointmentDTO> Appointments { get; set; }
        public AppointmentListDTO()
        {
            Appointments = new List<AppointmentDTO>();
        }
    }
}
