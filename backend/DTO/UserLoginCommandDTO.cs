﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.DTO
{
    public class UserLoginCommandDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
