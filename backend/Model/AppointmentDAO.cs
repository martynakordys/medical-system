﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalSystemBackend.Model
{
    public class AppointmentDAO
    {
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        public string Diagnose { get; set; }
        public int StatusId { get; set; }
        public AppointmentStatusDAO Status { get; set; }
        [Required]
        [Column(TypeName = "Date")]
        public DateTime RegistrationDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime FinishedCancelledDate { get; set; }

        public int PatientId { get; set; }
        public PatientDAO Patient { get; set; }

        public int RegistrantId { get; set; }
        public RegistrantDAO Registrant { get; set; }

        public int DoctorId { get; set; }
        public DoctorDAO Doctor { get; set; }

        public List<LaboratoryTestDAO> LaboratoryTests { get; set; }
        public List<PhysicalExaminationDAO> PhysicalExaminations { get; set; }
    }
}
