﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MedicalSystemBackend.Model
{
    public class ExaminationDAO
    {
        [Key]
        public string Code { get; set; }
        public int TypeId { get; set; }
        public ExaminationTypeDAO Type { get; set; }
        public string Name { get; set; }

    }
}
