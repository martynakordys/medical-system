﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Model
{
    public class LaboratoryAssistantDAO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public UserDAO User { get; set; }

        public List<LaboratoryTestDAO> LaboratoryTests { get; set; }
    }
}
