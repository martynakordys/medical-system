﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Model
{
    public class PatientDAO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }

        public string PersonalIdentityNumber { get; set; }

        public List<AppointmentDAO> Appointments { get; set; }
    }
}
