﻿namespace MedicalSystemBackend.Model
{
    public class UserDAO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }

        public int RoleId { get; set; }
        public UserRoleDAO Role { get; set; }
    }
}
