﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Model
{
    public class UserRoleDAO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDAO> Users { get; set; }
    }
}
