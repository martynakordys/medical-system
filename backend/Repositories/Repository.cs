﻿using System.Collections.Generic;

namespace MedicalSystemBackend.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly MedicalSystemDbContext context;

        public Repository(MedicalSystemDbContext context)
        {
            this.context = context;
        }
        public T Add(T tObject)
        {
            this.context.Set<T>().Add(tObject);
            this.context.SaveChanges();
            return tObject;
        }

        public T Delete(int Id)
        {
            T tObject = this.context.Set<T>().Find(Id);
            if (tObject != null)
            {
                this.context.Set<T>().Remove(tObject);
                this.context.SaveChanges();
            }

            return tObject;
        }

        public T Get(int Id)
        {
            return this.context.Set<T>().Find(Id);
        }

        public IEnumerable<T> GetAll()
        {
            return this.context.Set<T>();
        }

        public T Update(T tObjectChanges)
        {
            context.Entry(tObjectChanges).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            var tObject = this.context.Set<T>().Attach(tObjectChanges);
            tObject.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            this.context.SaveChanges();
            return tObjectChanges;
        }
    }
}
