﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace MedicalSystemBackend.Services
{
    public class AppointmentService:IAppointmentService
    {
        private readonly IRepository<AppointmentDAO> appointmentsRepository;
        private readonly IRepository<UserDAO> usersRepository;
        private readonly IRepository<DoctorDAO> doctorsRepository;
        private readonly IRepository<RegistrantDAO> registrantsRepository;
        private readonly IRepository<PatientDAO> patientsRepository;
        private readonly IRepository<AppointmentStatusDAO> appoinmentStatusRepository;



        public AppointmentService(
          IRepository<AppointmentDAO> appointmentsRepository,
          IRepository<UserDAO> usersRepository,
          IRepository<DoctorDAO> doctorsRepository,
          IRepository<RegistrantDAO> registrantsRepository,
          IRepository<PatientDAO> patientsRepository,
          IRepository<AppointmentStatusDAO> appoinmentStatusRepository)
        {
            this.appointmentsRepository = appointmentsRepository;
            this.usersRepository = usersRepository;
            this.doctorsRepository = doctorsRepository;
            this.registrantsRepository = registrantsRepository;
            this.patientsRepository = patientsRepository;
            this.appoinmentStatusRepository = appoinmentStatusRepository;
        }
        public AppointmentListDTO GetAppointmentsByDate(DateTime date)
        {
            IEnumerable<AppointmentDAO> appointments = appointmentsRepository.GetAll();
            AppointmentListDTO appointmentlistDTO = new AppointmentListDTO();
            foreach (AppointmentDAO appointment in appointments)
            {
                if (appointment.RegistrationDate == date)
                {
                    UserDAO doctor = usersRepository.Get(doctorsRepository.Get(appointment.DoctorId).UserId);
                    UserDAO registrant = usersRepository.Get(registrantsRepository.Get(appointment.RegistrantId).UserId);
                    PatientDAO patient = patientsRepository.Get(appointment.PatientId);
                    appointmentlistDTO.Appointments.Add(new AppointmentDTO
                    {
                        Id = appointment.Id,
                        Description = appointment.Description,
                        Diagnose = appointment.Diagnose,
                        Status = appoinmentStatusRepository.Get(appointment.StatusId).Name,
                        FinishedCancelledDate = appointment.FinishedCancelledDate.ToString("yyyy-MM-dd"),
                        DoctorName = doctor.FirstName + " " + doctor.Surname,
                        PatientName = patient.FirstName + " " + patient.Surname,
                        RegistrantName = registrant.FirstName + " " + registrant.Surname
                    });
                }
            }
            return appointmentlistDTO;
        }
        public AppointmentListDTO GetDoctorsAppointmentsByDate(DateTime date, int doctorId)
        {
            IEnumerable<AppointmentDAO> appointments = appointmentsRepository.GetAll();
            AppointmentListDTO appointmentlistDTO = new AppointmentListDTO();
            foreach (AppointmentDAO appointment in appointments)
            {
                if (appointment.RegistrationDate == date&& appointment.DoctorId==doctorId)
                {
                    UserDAO registrant = usersRepository.Get(registrantsRepository.Get(appointment.RegistrantId).UserId);
                    PatientDAO patient = patientsRepository.Get(appointment.PatientId);
                    appointmentlistDTO.Appointments.Add(new AppointmentDTO
                    {
                        Id = appointment.Id,
                        Description = appointment.Description,
                        Diagnose = appointment.Diagnose,
                        Status = appoinmentStatusRepository.Get(appointment.StatusId).Name,
                        FinishedCancelledDate = appointment.FinishedCancelledDate.ToString("yyyy-MM-dd"),
                        PatientName = patient.FirstName + " " + patient.Surname,
                        RegistrantName = registrant.FirstName + " " + registrant.Surname
                    });
                }
            }
            return appointmentlistDTO;
        }
    }
}
