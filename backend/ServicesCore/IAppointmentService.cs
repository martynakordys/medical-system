﻿using MedicalSystemBackend.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IAppointmentService
    {
        AppointmentListDTO GetAppointmentsByDate(DateTime date);
        AppointmentListDTO GetDoctorsAppointmentsByDate(DateTime date, int doctorId);


    }
}
