﻿
namespace MedicalSystemBackend.ServicesCore
{
    public interface IUserProviderService
    {
        int GetUserId();
    }
}
