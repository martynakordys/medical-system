﻿using MedicalSystemBackend.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IUserRoleService
    {
        RoleListDTO GetRoles();

    }
}
