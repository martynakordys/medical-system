﻿namespace MedicalSystemBackend.Settings
{
    public interface IDevelopmentSettings
    {
        bool IsDevelopment { get; set; }
    }
}