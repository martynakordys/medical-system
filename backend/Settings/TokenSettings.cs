﻿namespace MedicalSystemBackend.Settings
{
    public class TokenSettings : ITokenSettings
    {
        public string SecretKey { get; set; }
    }
}
